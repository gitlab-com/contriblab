## ContribLab

ContribLab is a software suite that provides contributor tracking capabilities and more.

### Information

It tracks the following contributor information:

* Location (Address)
* Email
* Contributions list

### Contributing

contriblab is an open source project and we are very happy to accept community
contributions. Please refer to [CONTRIBUTING.md](/CONTRIBUTING.md) for details.

### Sections

#### Public

* Contributor page (with contributor profiles)
  * Email form (self-service contributor contact form)
* Contributor profile pages
* Statistics page (how many, churn) with leaderboard

#### Contributor-only

* Profile update page

#### Admin

* Contributor account creation page
* Contributor team email (emails every contributor)

### Extra Functionality

* Automatically fetch contributions
* Match events to nearby contributors
* Ability to send gifts

### Feature Priority List

* Where contributors are located

Description: Keep note of which region our contributors are located

Use-Case: If there's a regional event that's nearby our contributor we can bring it up if he wants to attend it

* Contributor page

Description: A page which has a list of all the contributors (paginated) with their basic info - Region, GitLab handle

Use-Case: A person wants to check out a list of contributors, see if there's someone near him, contact someone on GitLab or check some sample contributions

* Contributor page email form

Description: A form where you can select which contributor you want to email. This is public.

Use-Case: Someone wants to contact a specific contributor. They can do it through this form without the need for exposing the contributors email to the public.

* Email everyone form

Description: A form which you can email all contributors with. This is private (admin-only).

Use-Case: When we (GitLab) want to issue a contributor-wide announcement or ask for feedback, ...

* Contribution tracking

Description: A list of commits / MRs where you can track which contributions has each contributor made.

Use-Case: We (GitLab) can closely track what has been contributed by the community.

* Contributor account creation - updating

Description: A form / profile page where contributors can update their profile with their info (email, name, image, GitLab handle, Twitter handle, ...)

Use-Case: Contributors can update their info in case something changed, or if we created their accounts with a mistake.

* Automatic contribution fetching

Description: Automatically fetch contributions by each contributor and add them to their profile.

Use-Case: The amount of work needed for tracking contributions is drastically reduced since it's automated.

* Event matching

Description: A form that matches regional events to local contributors.

Use-Case: We can promote regional events and contributors sharing their experience at them.

* Gift sending

Description: A button or functionality that makes it easy to send gifts to contributors.

Use-Case: We can easily reward contributors for their contributions.

* Statistics

Description: Statistics on which project has been contributed to the most, when was the most active period for contributions, etc...

Use-Case: We can visualize the efectivness of our community programs and strategize new ones.

* Leaderboard

Description: A leaderboard that lists the people who contributed the most (calculating in the "size" of the contribution)

Use-Case: People can "compete" and have a "hall-of-fame" contributors.

